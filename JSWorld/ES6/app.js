import React from 'react';
import ReactDOM from 'react-dom/client';

const myArr = ['apple', 'banana', 'orange'];

const myList = myArr.map((fruit) => <h1>{fruit}</h1>);

ReactDOM.render(myList, document.getElementById('root'));